from wtforms import Form, validators, StringField, TextAreaField, PasswordField

class RegisterForm(Form):

    username = StringField("Username",[validators.Length(min=4,max=15)])
    email = StringField("Email",[validators.Length(min=6,max=25)])
    password = PasswordField("Password",[
        validators.DataRequired(),
        validators.EqualTo("confirm", message="Passwords do not match.")
    ])
    confirm = PasswordField("Confirm Password", [validators.DataRequired()])

    