import sqlite3

class User:

    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        self.dbPath = "database/database.db"



    def insertUser(self,username,password,email):

        with sqlite3.connect(self.dbPath) as con:

            cur = con.cursor()
            cur.execute("INSERT INTO users (username,password,email) VALUES (?,?,?)",(username,password,email))
            con.commit()

