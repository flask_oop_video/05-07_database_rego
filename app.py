from flask import Flask, render_template, request, redirect, url_for
from passlib.hash import sha256_crypt
from classes.user import User
from config import Config
import classes.forms


app = Flask(__name__)

@app.route("/")
def hello():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")



@app.route("/user/<string:name>")
def user(name):
    return render_template("user.html", name=name)



@app.route("/register", methods=["GET", "POST"])
def register():

    form = classes.forms.RegisterForm(request.form)

    if request.method == "POST" and form.validate():

        username = form.username.data
        email = form.email.data
        password = sha256_crypt.encrypt(str(form.password.data))

        user = User()
        user.insertUser(username,password,email)

        return redirect(url_for("login"))

    return render_template("register.html", form=form)


@app.route("/login")
def login():
    return render_template("login.html")




app.run(debug=True)